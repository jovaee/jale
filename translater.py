import urllib
import urllib2

import pyglet

from gtts import gTTS
from BeautifulSoup import BeautifulSoup


class Translater:
    """
    
    """

    SUPPORTED_LANG = [
        'af', 'ga', 'sq', 'it', 'ar', 'ja', 'az', 'kn', 'eu', 'ko', 'bn', 'la', 'be', 'lv', 'bg', 'lt', 'ca', 'mk', 'ms',
        'cy', 'mt', 'yi', 'hr', 'no', 'cs', 'fa', 'da', 'pl', 'nl', 'pt', 'en', 'ro', 'eo', 'ru', 'et', 'sr', 'tl', 'sk',
        'fi', 'sl', 'fr', 'es', 'gl', 'sw', 'ka', 'sv', 'de', 'ta', 'el', 'te', 'gu', 'th', 'ht', 'tr', 'iw', 'uk', 'hi',
        'ur', 'hu', 'vi', 'is', 'id'
    ]
    
    def __init__(self):
        self.link = "http://translate.google.com/?hl=%s&sl=%s&q=%s"
        self.agent = {'User-Agent': "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"}
     
    def translate_text(self, text, from_lang='en', to_lang='ja'):
        """
        
        :param text:
        :param from_lang:
        :param to_lang:
        :return: Translated text
        """

        if from_lang not in self.SUPPORTED_LANG:
            raise AttributeError("Given source language is not supported")
        elif to_lang not in self.SUPPORTED_LANG:
            raise AttributeError("Given destination language is not supported")

        to_translate = urllib.quote_plus(text)
        link = self.link % (to_lang, from_lang, to_translate)
        request = urllib2.Request(link, headers=self.agent)

        page = urllib2.urlopen(request).read()

        # print page

        html = page
        parsed_html = BeautifulSoup(html)
        
        # This gets the translated text as roman characters
        aa = parsed_html.body.find('div', attrs={'class': 'translit', 'id': 'res-translit'})

        # This get the translated text in their native format
        bb = parsed_html.body.find('span', attrs={'title': text})

        return aa.text, bb.text
    
    def text_to_speech(self, text, lang='ja'):
        """
        
        :param text:
        :param lang:
        :return:
        """

        filename = '%s.mp3'
        
        tts = gTTS(text, lang)

        tts.save('apple.mp3')

        pyglet.lib.load_library('avbin')
        pyglet.have_avbin = True

        explosion = pyglet.media.load('apple.mp3')
        explosion.play()

        def exit_callback(dt):
            pyglet.app.exit()

        pyglet.clock.schedule_once(exit_callback, explosion.duration)
        pyglet.app.run()

    def save_speech_to_file(self):
        """
        
        :return:
        """
        pass
    
    def full_circle(self):
        """
        
        :return:
        """
        pass
