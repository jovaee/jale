import cmd
import json
import re

from translater import Translater


def start_cli():
    CLI().cmdloop()


def upper_first(x):
    """

    :param x:
    :return:
    """
    return x[0].upper() + x[1:]


class CLI(cmd.Cmd):
    """

    """

    to_lang = 'ja'
    from_lang = 'en'

    class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

    def __init__(self):
        # Call super init
        cmd.Cmd.__init__(self)

        self.translater = Translater()

        with open('dic.json', 'r') as f:
            self.dic = json.load(f)

    def do_EOF(self, line):
        """

        :param line:
        :return:
        """
        return True

    def print_translation(self, id, org):
        """

        :param id:
        :param org:
        :return:
        """

        # This try except block is necessary since it will raise exception if the unicode characters are not installed on the sytsem
        try:
            print "%2s Text : %s" % (self.bcolors.OKBLUE + self.from_lang.upper(), unicode(self.dic[id][org.lower()][self.from_lang]))
            print "%2s Text : %s" % (self.bcolors.OKGREEN + self.to_lang.upper(), unicode(self.dic[id][org.lower()][self.to_lang]))


            if self.to_lang + "-native" in self.dic[id][org.lower()]:
                print "%2s Native Text : %s" % (self.bcolors.WARNING + self.to_lang.upper(), unicode(self.dic[id][org.lower()][self.to_lang + "-native"]))
        except UnicodeEncodeError:
            print 'ERROR: The translation contains unicode characters that your system cannot display'

        # Set printing color back to normal
        print '\033[0m'

    def complete_translate(self, text, line, begidx, endidx):
        id = '%s-%s' % (self.from_lang, self.to_lang)

        if not text:
            return self.dic[id].keys()
        else:
            matches = []

            for item in self.dic[id].keys():
                if text in item:
                    matches.append(item)

            return matches

    def do_translate(self, text):
        """

        :param text:
        :return:
        """
        id = '%s-%s' % (self.from_lang, self.to_lang)

        if id not in self.dic or text.lower() not in self.dic[id]:
            try:
                trans, fancy_trans = self.translater.translate_text(text, from_lang=self.from_lang, to_lang=self.to_lang)

                # For some reason ' is not returned correctly by google translate, so replace it with regex
                trans = re.sub("&#39;", "'", trans)

                # If the language entry does not exist create it
                if id not in self.dic:
                    self.dic[id] = {}

                # If the word entry does not exist create it
                if text.lower() not in self.dic[id]:
                    self.dic[id][text.lower()] = {}

                # Add new translation into memory dictionary
                self.dic[id][text.lower()][self.from_lang] = upper_first(text)
                self.dic[id][text.lower()][self.to_lang] = trans

                if fancy_trans:
                    self.dic[id][text.lower()][self.to_lang + "-native"] = fancy_trans

                # Write update memory dictionary to file
                self.update_dic()

            except AttributeError as ae:
                print ae

        # Finally print the translation
        self.print_translation(id, text)

    def update_dic(self):
        """

        :return:
        """
        with open('dic.json', 'w') as f:
            json.dump(self.dic, f, indent=2, sort_keys=True)

    def do_setSourceLang(self, text):
        """

        :param text:
        :return:
        """
        self.from_lang = text

    def do_setDestLang(self, text):
        """

        :param text:
        :return:
        """
        self.to_lang = text
